﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using IndigoSpace.Models.PropertyModels;

namespace IndigoSpace.Models
{
    public class RabbitApiResponse
    {
        public RabbitApiResponse(Guid requestId, string[] messageType, string destinationAddress,
               string faultAddress, Dictionary<string, object> message, string responseAddress, string sourceAddress)
        {
            RequestId = requestId;
            MessageTypes = new MessageType[messageType.Length];
            for (var i = 0; i < messageType.Length; i++)
            {
                MessageTypes[i] = new MessageType(messageType[i]);
            }
            MessageProperties = message.ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            object cid;
            if (MessageProperties.TryGetValue("correlationid", out cid))
            {
                CorrelationId = new Guid((string)cid);
                MessageProperties.Remove("correlationid");
            }
            else
            {
                CorrelationId = Guid.Empty;
            }
            DestinationAddress = destinationAddress;
            FaultAddress = faultAddress;


            ResponseAddress = responseAddress;
            SourceAddress = sourceAddress;
            TimeReceived = DateTime.Now.TimeOfDay;

            string timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                            CultureInfo.InvariantCulture);
            var ft = DateTime.Parse(timestamp, CultureInfo.InvariantCulture);
        }

        public Guid RequestId { get; set; }
        public MessageType[] MessageTypes { get; set; }
        public Guid CorrelationId { get; set; }
        public string DestinationAddress { get; set; }
        public string FaultAddress { get; set; }
        public Dictionary<string, object> MessageProperties { get; set; }
        public string ResponseAddress { get; set; }
        public string SourceAddress { get; set; }
        public TimeSpan TimeReceived { get; set; }
    }
}