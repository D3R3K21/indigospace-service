﻿namespace IndigoSpace.Models.PropertyModels
{
    public class MessageType
    {
        public MessageType(string type)
        {
            var trimmed = type.Replace("urn:message:", "").Split(':');
            Namespace = trimmed[0];
            MessageName = trimmed[1];
        }
        public string Namespace { get; protected set; }
        public string MessageName { get; protected set; }
    }
}