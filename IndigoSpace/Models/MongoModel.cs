﻿using System;
using System.Collections.Generic;

namespace IndigoSpace.Models
{
    public class MongoModel
    {
        public Guid CorrelationId { get; set; }
        public List<RabbitApiResponse> Correlations { get; set; }

        public MongoModel()
        {
            Correlations = new List<RabbitApiResponse>();
        }
    }
}