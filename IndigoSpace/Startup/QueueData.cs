﻿using System.Threading.Tasks;

namespace IndigoSpace
{
    public static class QueueData
    {
        private static ServiceTask _taskService;
        private static Task _task;
        static QueueData()
        {
            Refresh();
        }

        public static void Initialize()
        {
        }

        public static void Refresh()
        {
            //TODO: make this thread safe
            if (_taskService != null)
            {
                _taskService.SetCanceled();
                _taskService.Task.Dispose();
            }

            _task =
            Task.Factory.StartNew(() =>
            {
                _taskService = new ServiceTask();
            })
            .ContinueWith((x, y) =>
            {
                x.Dispose();
                x = null;
            }, _task);

        }
    }
}