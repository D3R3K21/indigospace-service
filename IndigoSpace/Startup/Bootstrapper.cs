﻿using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace IndigoSpace
{

    public class Bootstrapper : DefaultNancyBootstrapper
    {

        public Bootstrapper()
        {

        }
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            QueueData.Initialize();
            base.ApplicationStartup(container, pipelines);
        }
    }
}