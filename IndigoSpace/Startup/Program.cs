﻿using System.Threading;
using System;
using CommandLine;
using CommandLine.Text;
using IndigoSpace.Properties;
using Nancy.Hosting.Self;

namespace IndigoSpace
{
    class Options
    {
        //8080
        [Option('p', "port", DefaultValue = 8181, HelpText = "Port for service to run on.")]
        public int Port { get; set; }

        //mongodb://dev.docker:27017
        [Option('s', "storage", Required = true, HelpText = "Connection string for storage.")]
        public string StorageConnectionString { get; set; }

        //dev.docker
        [Option('q', "queue", Required = true, HelpText = "Connection string for message queue.")]
        public string MessageQueue { get; set; }

        [Option('a', "adminport", DefaultValue = 15672, HelpText = "Port for Rabbit Admin port.")]
        public int QueueAdminPort { get; set; }

        [Option('v', "verbose", DefaultValue = false, HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }



        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine(Settings.Default.LOL);
            var options = new Options();
            if (Parser.Default.ParseArguments(args, options))
            {
                Settings.Default.Mongo = options.StorageConnectionString;
                Settings.Default.Endpoint = string.Format("http://localhost:{0}", options.Port);
                Settings.Default.Queue = options.MessageQueue;
                Settings.Default.Verbose = options.Verbose;
                Settings.Default.AdminPort = options.QueueAdminPort;

                if (options.Verbose)
                {
                    Console.WriteLine("{0} {1} {2}", new string('=', 16), "Verbose Logging", new string('=', 16));
                    Console.WriteLine("Service Endpoint: {0}", Settings.Default.Endpoint);
                    Console.WriteLine("Mongo Endpoint: {0}", Settings.Default.Mongo);
                    Console.WriteLine("Queue URI: {0}", Settings.Default.Queue);
                    Console.WriteLine("Verbose Enabled: {0}", Settings.Default.Verbose);
                    Console.WriteLine("Admin Port: {0}", Settings.Default.AdminPort);
                    Console.WriteLine("{0}{1}", new string('=', 49), "\n\n");
                }

            }
            else
            {
                Environment.Exit(-1);
            }



            var uri = new Uri(Settings.Default.Endpoint);
            var bootstrapper = new Bootstrapper();
            var hostConfig = new HostConfiguration { UrlReservations = new UrlReservations { CreateAutomatically = true } };

            using (var host = new NancyHost(uri, bootstrapper, hostConfig))
            {
                host.Start();
                Console.WriteLine("Your application is running on " + uri);
                Thread.Sleep(Timeout.Infinite);
            }
        }
    }
}
