﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IndigoSpace.Models;
using IndigoSpace.Properties;
using IndigoSpace.Storage;
using RabbitMQ.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IndigoSpace
{
    public class ServiceTask : TaskCompletionSource<object>
    {
        //TODO: add these as settings
        private const string Queue = "IndigoSpace_Queue";
       // private const string Host = "dev.docker";
        private readonly List<string> _exchanges;
        private string _managementUri
        {
            get
            {
                return string.Format("http://{0}:{1}/api/definitions", Settings.Default.Queue, Settings.Default.AdminPort);
            }
        }
        private static readonly ConnectionFactory Factory = new ConnectionFactory
        {
            HostName = Settings.Default.Queue
        };

        public ServiceTask()
        {
            _exchanges = GetExchanges();
            Listen();
        }

        private List<string> GetExchanges()
        {
            var exchanges = new List<string>();
            var request = WebRequest.Create(new Uri(_managementUri));
            request.Method = "GET";
            request.Credentials = new NetworkCredential("guest", "guest");

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            throw new WebException();
                        }
                        using (var reader = new StreamReader(stream))
                        {
                            dynamic jsonObj = JsonConvert.DeserializeObject(reader.ReadToEnd());
                            exchanges = ((JArray)jsonObj.exchanges).Select(p => p.Value<string>("name")).ToList();

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                //log exception, bubble up or restart?
                throw;
            }

            return exchanges;
        }
        private void Listen()
        {
            try
            {
                using (var connection = Factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {

                        channel.QueueDeclare(Queue, true, false, false, null);
                        _exchanges.ForEach(p =>
                        {
                            channel.QueueBind(Queue, p, "");
                            Console.Out.WriteLine("{0} bound to exchange: {1}", Queue, p);

                        });


                        var consumer = new EventingBasicConsumer(channel);
                        var repo = new CorrelationRepository(Settings.Default.Mongo);
                        EventHandler<BasicDeliverEventArgs> handler = (model, ea) =>
                        {
                            var response = JsonConvert.DeserializeObject<RabbitApiResponse>(Encoding.UTF8.GetString(ea.Body));

                            channel.BasicAck(ea.DeliveryTag, false);
                            repo.Save(response);
#if DEBUG
                            Console.WriteLine(new string('-', 20));
                            Console.WriteLine(" [x] Correlation Id: {0}", response.CorrelationId);
                            response.MessageTypes.ToList().ForEach(x =>
                            {
                                //Console.WriteLine("MessageProperties Type: {0}", x.Replace("urn:message:", ""));
                                Console.WriteLine("MessageProperties Name: {0}", x.MessageName);
                                Console.WriteLine("MessageProperties Namespace: {0}", x.Namespace);
                            });

#endif
                        };
                        consumer.Received += handler;

                        channel.BasicConsume(Queue, false, consumer);
                        //TODO: loop and refresh? 
                        Thread.Sleep(Timeout.Infinite);

                    }
                }
            }
            catch (Exception ex)
            {
                //log exception, bubble up or restart?
                throw;
            }
        }
    }
}