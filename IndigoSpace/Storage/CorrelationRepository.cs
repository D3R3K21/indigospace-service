﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IndigoSpace.Models;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace IndigoSpace.Storage
{
    public class CorrelationRepository
    {
        private const string DbName = "IndigoSpace";
        private const string CollectionName = "RegisteredCorrelations";

        private IMongoClient Client { get; set; }
        private IMongoDatabase Database { get; set; }
        private IMongoCollection<MongoModel> Collection { get; set; }

        public CorrelationRepository(string connectionString)
        {


            if (!BsonClassMap.IsClassMapRegistered(typeof(MongoModel)))
            {
                BsonClassMap.RegisterClassMap<MongoModel>(map =>
                {
                    map.AutoMap();
                    var memberMap = map.GetMemberMap(x => x.CorrelationId);
                    memberMap.SetIdGenerator(GuidGenerator.Instance);
                    map.SetIdMember(memberMap);

                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(RabbitApiResponse)))
            {
                BsonClassMap.RegisterClassMap<RabbitApiResponse>(map =>
                {
                    map.AutoMap();
                    map.UnmapProperty(p => p.CorrelationId);

                });
            }

            Client = new MongoClient(connectionString);
            Database = Client.GetDatabase(DbName);
            Collection = Database.GetCollection<MongoModel>(CollectionName);
        }

        public void Save(RabbitApiResponse doc)
        {
            var existingDoc = GetOne(p => p.CorrelationId == doc.CorrelationId);

            if (existingDoc == null)
            {
                existingDoc = new MongoModel
                {
                    CorrelationId = doc.CorrelationId,
                    Correlations = { doc }
                };
                Collection.InsertOne(existingDoc);
            }
            else
            {
                existingDoc.Correlations.Add(doc);
                Collection.FindOneAndReplace(p => p.CorrelationId == doc.CorrelationId, existingDoc);
            }

        }
        public List<MongoModel> GetMany(Expression<Func<MongoModel, bool>> filter)
        {
            return Collection.Find(filter).ToList();
        }
        public MongoModel GetOne(Expression<Func<MongoModel, bool>> filter)
        {
            return Collection.Find(filter).FirstOrDefault();
        }
    }
}